# Py-Pong!

## Overview

Pong! The classic game built in Python!

This project was my first unguided project I made in Python. I used this project to continue to build my foundations in Python and also to learn to use version control systems like Git. 

## Features
* Keyboard input
* Sound
* Computer AI (available via command line parameters)

## Installation Instructions
Currently, I have made no attempt to create a standalone executable, so the game can only be run by installing Python and running py-pong.py from the command line.

## Requirements
* Python 3.8
* Pygame 2.0.1
(The above versions listed were the last to be tested and known working with the project.)

## Command line parameters
* `--computer | -cpu` (boolean) Have the Computer play as Player 2.
* `--difficulty | -d` (float) Sets the difficulty level of the CPU player. (default: 0.75, range: 0.00 to 1.00)

## Contributors
* VGpunx - Primary contributor
* Taylor-MadeAK - Computer player AI